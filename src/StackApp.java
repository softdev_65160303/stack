public class StacksQueues {

    public static void main(String[] args) {
        Stack theStack = new Stack(10);

        for (long value : new long[] { 20, 40, 60, 80 }) {
            theStack.push(value);
        }

        while (!theStack.isEmpty()) {
            long value = theStack.pop();
            System.out.print(value + " ");
        }
        System.out.println();
    }
}
